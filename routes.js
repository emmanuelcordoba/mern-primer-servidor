const express = require('express');
const router = express.Router();
const usuarioController = require('./controllers/usuarioController');
const categoriaController = require('./controllers/categoriaController');
const pagoController = require('./controllers/pagoController');
const authController = require('./controllers/authController');
const auth = require('./middlewares/auth');
const { check } = require('express-validator');

// Autentiar usuario
// api/auth/
router.post('/auth/',
    [
        check('email','El email es obligatorio.').notEmpty(),
        check('email','Ingrese un email válido.').isEmail(),
        check('password','El password es obligatorio.').notEmpty(),
    ],
    authController.login
)

// Crear usuarios
// api/usuarios/
router.post('/usuarios/',
    [
        check('nombre','El nombre es obligatorio.').notEmpty(),
        check('email','El email es obligatorio.').notEmpty(),
        check('email','Ingrese un email válido.').isEmail(),
        check('password','El password es obligatorio.').notEmpty(),
        check('password','El password debe ser de al menos 6 caracteres.').isLength({ min: 6}),
    ],
    usuarioController.crear
);

// Crear categoria
// api/categorias/
router.post('/categorias/',
    auth,
    [
        check('nombre','El nombre es obligatorio.').notEmpty()
    ],
    categoriaController.crear
);

// Listar categorías
// api/categorias/
router.get('/categorias/',
    auth,
    categoriaController.listar
);

// Eliminar Categoría
// api/categorias/:id
router.delete('/categorias/:id',
    auth,
    categoriaController.eliminar
);

// Crear pagos
// api/pagos/
router.post('/pagos/',
    auth,
    [
        check('titulo','El titulo es obligatorio.').notEmpty(),
        check('monto','El monto es obligatorio.').notEmpty(),
        check('monto','Ingrese un monto válido.').isFloat({min: 1}),
        check('categoria','La categoria es obligatoria.').notEmpty(),
        check('categoria','La categoria no es válida.').isMongoId()
    ],
    pagoController.crear
);

// Listar pagos por categoria
// api/pagos/:categoria_id/listar
router.get('/pagos/:categoria_id/listar',
    auth,
    pagoController.listarPorCategoria
);


// Actualizar Pago
// api/pagos/:id
router.put('/pagos/:id',
    auth,
    [
        check('titulo','El titulo es obligatorio.').notEmpty(),
        check('monto','El monto es obligatorio.').notEmpty(),
        check('monto','Ingrese un monto válido.').isFloat({min: 1}),
        check('categoria','La categoria es obligatoria.').notEmpty(),
        check('categoria','La categoria no es válida.').isMongoId()
    ],
    pagoController.actualizar
);

// Eliminar Pago
// api/pagos/:id
router.delete('/pagos/:id',
    auth,
    pagoController.eliminar
);

module.exports = router;