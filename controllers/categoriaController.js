const Categoria = require('../models/Categoria');
const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

// Crear Categoría
exports.crear = async (req, res) => {

    // Validación de campos
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        // Creamos la categoria
        let categoria = new Categoria(req.body);
        categoria.usuario = req.usuario.id

        // Guardamos la categoria en la BD
        await categoria.save();

        res.json({ msg: 'Categoria creada correctamente.' });
        
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
};

// Listar Categorías
exports.listar = async (req, res) => {
    try {
        const categorias = await Categoria.find({ usuario: req.usuario.id }).sort('nombre');
        res.json({ categorias });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ msg: 'Hubo un error' });
    }
}

// Eliminar Categoría
exports.eliminar = async (req, res) => {
    try {
        // Validar ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'La Categoría no existe.'});
        }
 
        // Verificar que la Categoría exista
        let categoria = await Categoria.findById(req.params.id);
        
        if(!categoria){
            return res.status(404).json({ msg: 'La Categoría no existe.'});
        }
 
        // Verificar que la categoría le pertenezca al usuario autenticado
        if(categoria.usuario.toString() != req.usuario.id){
            return res.status(401).json({ msg: 'No autorizado.' });
        }
 
        // Eliminar
        await categoria.remove();        
        res.json({msg: 'Categoría eliminada correctamente.'});
 
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
};
