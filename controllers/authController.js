const Usuario = require('../models/Usuario');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

// Autenticar Usuario
exports.login = async (req, res) => {
    
    // Revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(400).json({ errors: errores.array() })
    }
    
    // Extraer email y password
    const { email, password } = req.body;

    try {
        // Revisar que sea un usuario registrado
        let usuario = await Usuario.findOne({ email });
        if(!usuario){
            console.log('Email no válido.');
            res.status(400).json({ msg: 'El email o contraseña no son válidos.'})
        }

        // Verificar la contraseña
        const passCorrecto = await bcryptjs.compare(password, usuario.password);
        if(!passCorrecto){
            console.log('Password no válido.');
            res.status(400).json({ msg: 'El email o contraseña no son válidos.'})
        }

        // TODO OK

        // Crear y firmar el JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };
 
        // Firmar token
        jwt.sign(payload, process.env.SECRET, {
            expiresIn: '1h'
        }, (error, token) => {
            if(error) throw error;
        
            // Mensaje de confirmación
            res.json({ token });
        });
        
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
};