const Pago = require('../models/Pago');
const Categoria = require('../models/Categoria');
const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

// Crear Categoría
exports.crear = async (req, res) => {

    // Validación de campos
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        // Controlar la categoria
        const categoria = await Categoria.findById(req.body.categoria);
        if(!categoria){
            res.status(400).json({ msg: 'La Categoria no existe.'});
        }

        // Validar si la categoria pertenece al usuario autenticado
        if(categoria.usuario.toString() !== req.usuario.id){
            res.status(401).json({ msg: 'No autorizado.' });
        }
        
        // Creamos el pago
        let pago = new Pago(req.body);

        // Guardamos el pago en la BD
        await pago.save();

        res.json({ msg: 'Pago creado correctamente' });
        
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error'});
    }
};

// Listar Pagos por Categoría
exports.listarPorCategoria = async (req, res) => {
    try {
        // Controlar la categoria
        const categoria = await Categoria.findById(req.params.categoria_id);
        if(!categoria){
            res.status(400).json({ msg: 'La Categoria no existe.'});
        }

        // Validar si la categoria pertenece al usuario autenticado
        if(categoria.usuario.toString() !== req.usuario.id){
            res.status(401).json({ msg: 'No autorizado.' });
        }

        // Buscar y listar los pagos
        const pagos = await Pago.find({ categoria: categoria.id });
        res.json({ pagos });

    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
};

// Actualizar Pago
exports.actualizar = async (req, res) => {

    // Validación de campos
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    // Extraer la categoría
    const { categoria } = req.body;

    try {
        // Validar ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'El Pago no existe.'});
        }

        // Controlar que el Pago exista
        let pago = await Pago.findById(req.params.id).populate('categoria');
        if(!pago){
            return res.status(404).json({ msg: 'El Pago no existe.'});
        }

        // Verificar que el Pago pertenezca al Usuario autenticado
        if(pago.categoria.usuario.toString() !== req.usuario.id){
            return res.status(401).json({ msg: 'No autorizado.'});
        }

        // Controlar la categoria
        const categoriaExiste = await Categoria.findById(categoria);
        if(!categoriaExiste){
            res.status(400).json({ msg: 'La Categoria no existe.'});
        }

        // Validar si la categoria pertenece al usuario autenticado
        if(categoriaExiste.usuario.toString() !== req.usuario.id){
            res.status(401).json({ msg: 'No autorizado.' });
        }

        // Actualizar
        pago = await Pago.findByIdAndUpdate(req.params.id, req.body, { new: true });

        res.json({ msg: 'El Pago fue actualizado.', pago});
        
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.' });
    }
};


// Eliminar Pago
exports.eliminar = async (req, res) => {
    try {
        // Validar ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'El Pago no existe.'});
        }

        // Controlar que el Pago exista
        let pago = await Pago.findById(req.params.id).populate('categoria');
        if(!pago){
            return res.status(404).json({ msg: 'El Pago no existe.'});
        }

        // Verificar que el Pago pertenezca al Usuario autenticado
        if(pago.categoria.usuario.toString() !== req.usuario.id){
            return res.status(401).json({ msg: 'No autorizado.'});
        }

        // Eliminar
        await pago.remove();
        res.json({ msg: 'Pago eliminado.'});

    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.' });
    }
};