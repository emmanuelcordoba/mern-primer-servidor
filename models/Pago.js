const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Definir Schema
const PagoSchema = new Schema({
    titulo : {
        type: String,
        required: true,
        trim: true
    },
    monto: {
        type: Number,
        required: true
    },
    categoria: {
        type: Schema.Types.ObjectId,
        ref: 'Categoria',
        required: true,
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
});

// Definimos el modelo con el Schema
module.exports = mongoose.model('Pago', PagoSchema);