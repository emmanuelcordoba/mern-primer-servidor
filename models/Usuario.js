const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Definir Schema
const UsuarioSchema = new Schema({
    nombre : {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    created_at: {
        type: Date,
        default: Date.now()
    }
});

// Definimos el modelo con el Schema
module.exports = mongoose.model('Usuario', UsuarioSchema);