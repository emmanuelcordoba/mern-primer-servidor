const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Pago = require('./Pago');

// Definir Schema
const CategoriaSchema = new Schema({
    nombre : {
        type: String,
        required: true,
        trim: true
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario'
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
});

// Elimino en cascada de los Pagos
CategoriaSchema.pre('remove', async function(){
    await Pago.deleteMany({ categoria: this._id}).exec();
});

// Definimos el modelo con el Schema
module.exports = mongoose.model('Categoria', CategoriaSchema);