require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('./database');
 
//Crear el servidor
const app = express();

// Configuraciones
const PORT = process.env.PORT || 4000;

// Middlewares
app.use(morgan('dev'));
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true})); // for parsing application/x-www-form-urlencoded

// Rutas
app.use('/api',require('./routes'));
 
// Iniciar el servidor
app.listen(PORT, () => {
    console.log(`El servidor está funcionando en el puerto ${PORT}`);
});